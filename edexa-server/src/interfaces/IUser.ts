import { Container } from 'typedi';
import * as jwt from 'jsonwebtoken';
import * as crypto from 'crypto-js';
import config from '../config';
import Logger from '../loaders/logger';
import Users from '../models/Users';
export class IUSers {
	static async signupUser(data: object) {
		try {
			let checkuser = await Users.findOne({ email: data['email'] });
			if (checkuser)
				return {
					status: 400,
					message: 'user already registred with this email',
				};
			let user = new Users(data);
			await user.save();

			const token_data = { userId: user['_id'], email: user['email'] };

			// Encrypt Token
			const token = await UserTokenGenerate(token_data);
			return {
				status: 200,
				message: 'register user Sucressfully',
				token: token,
			};
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
	}

	static async signInUser(data: object) {
		try {
			let user = await Users.findOne({ email: data['email'] });
			if (!user)
				return {
					status: 400,
					message: 'Invalid Email or passwordcdd.',
				};
			let checkpassword = await user.comparePassword(data['password']);
			if (!checkpassword) return {
				status: 400,
				message: 'Invalid Email or password.',
			};

			const token_data = { userId: user['_id'], email: user['email'] };

			// Encrypt Token
			const token = await UserTokenGenerate(token_data);
			return {
				status: 200,
				message: 'register user Sucressfully',
				token: token,
			};
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
	}

	static async userInfo() {
		const token:any = await Container.get('auth-token');
		try {
			let user = await Users.findOne({ _id: token['userId']});
			return {
				status: 200,
				data: user,
			};
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
	}
}

async function UserTokenGenerate(token_data: Object) {
	const ciphertext = crypto.AES.encrypt(JSON.stringify(token_data), config.aesSecretkey);

	//Sign JWT, valid as per env hour
	const token = jwt.sign({ sub: ciphertext.toString() }, config.jwtSecret, {
		expiresIn: config.jwtetl,
	});

	return token;
}