import { Container } from 'typedi';
import config from '../config';
import Logger from '../loaders/logger';
import Products from '../models/Products';
export class IProducts {
	static async addProductofUsers(data:object) {
        const token:any = await Container.get('auth-token');
		try {
            let exist_product = await Products.findOne({name:{$regex:data['name'],$options:'i'}});
            if(exist_product)return{
                status:400,
                message:'This name product already exists'
            }
            let product_data={
                name:data['name'],
                price:data['price'],
                user:token['userId']
            }
            let newProduct = new Products(product_data);
            let checksave = await newProduct.save();
            if(checksave)return{
                status:200,
                message:'Product Added Successfully.'
            }
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
    }

    static async editProductofUsers(data:object,product_id:string) {
        const token:any = await Container.get('auth-token');
		try {
            let exist_product = await Products.findOne({_id:product_id,user:token['userId']});
            if(!exist_product)return{
                status:404,
                message:'Product not found.'
            }
            let name_exists = await Products.findOne({name:{$regex:data['name'],$options:'i'},_id:{$ne:product_id}});
            if(name_exists)return{
                status:400,
                message:'This name product already exists'
            }

            let product_data={};
            if(data['name']){
                Object.assign({name:product_data});
            }
            if(data['price']){
                Object.assign({price:data['price']});
            }
           let updateProduct = await Products.findOneAndUpdate({_id:product_id},product_data);
            return{
                status:200,
                message:'Product Edit Successfully.'
            }
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
    }

    static async deleteProductById(product_id:string) {
		try {
			let exist_product = await Products.findOne({_id:product_id});
            if(!exist_product)return{
                status:404,
                message:'Product not found.'
            }
            await Products.deleteOne({_id:product_id})
            return{
                status:200,
                message:'product deleted successfully.'
            }
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
    }
    
    static async getProductById(product_id:string) {
		try {
			let exist_product = await Products.findOne({_id:product_id});
            if(!exist_product)return{
                status:404,
                message:'Product not found.'
            }
            return{
                status:200,
                data:exist_product
            }
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
    }
    
    static async myProductListing() {
        const token:any = await Container.get('auth-token');
		try {
            let products = await Products.find({user:token['userId']});

            if(products.length == 0)return{
                status:404,
                message:'Product not found.'
            }
           
            return{
                status:200,
                data:products
            }
		} catch (error) {
			Logger.error(error)
			return { status: 500, message: 'Something went wrong' };
		}
    }
}
