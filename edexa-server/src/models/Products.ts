import { Document, Model, model, Schema } from 'mongoose';

export interface IProducts extends Document {
  name: string;
  price;
}

const productSchema: Schema = new Schema(
  {
    name: {
      type: String,
      unique: true,
    },
    price: {
      type: Number,
    },
    user:{
      type: Schema.Types.ObjectId,
      default: null,
      ref: 'Users',
    }
  },
  {
    timestamps: true,
  },
);

const Products: Model<IProducts> = model<IProducts>('Products', productSchema);

export default Products;
