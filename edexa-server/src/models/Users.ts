import { Document, Model, model, Schema } from 'mongoose';
import bcrypt from 'bcryptjs';
const SALT_WORK_FACTOR = 10;

export interface IUsers extends Document {
  name: string;
  email: string;
  password: string;
  comparePassword(password: string): boolean;
}

const userSchema: Schema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
    },
    password: {
      type: String,
    }
  },
  {
    timestamps: true,
  },
);

userSchema.pre('save', async function (next) {
  let user: any = this;
  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  let encryppassword = await bcrypt.hash(user.password, salt);
  user.password = encryppassword;
  next();
});

userSchema.methods.comparePassword = function (password: string) {
  if (bcrypt.compareSync(password, this.password)) return true;
  return false;
};

const Users: Model<IUsers> = model<IUsers>('Users', userSchema);

export default Users;
