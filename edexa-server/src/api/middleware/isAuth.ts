import { Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import * as jwt from 'jsonwebtoken';
import config from '../../config';
import * as crypto from 'crypto-js';

export const isAuth = (req: Request, res: Response, next: NextFunction) => {
    //Get the jwt token from the head
    const token = <string>req.headers['authorization'];
    //Try to validate the token and get data
            jwt.verify(token, config.jwtSecret, function (err, decode) {
                if (err) {
                    return res.status(401).json({ status: 401, message: 'Invalid auth token' });
                }

                res.locals.jwtTTL = { exp: decode.exp, jwt: token };
                const bytes = crypto.AES.decrypt(decode.sub, config.aesSecretkey);
                const decryptedData = bytes.toString(crypto.enc.Utf8);

                res.locals.jwtPayload = JSON.parse(decryptedData);
                Container.set('auth-token', res.locals.jwtPayload);
                next();
            });
};