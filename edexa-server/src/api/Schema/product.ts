import { celebrate, Joi } from 'celebrate';

const PRODUCT_SCHEMAS = {
    ADD_PRODUCT: celebrate({
        body: Joi.object({
            name:Joi.string(),
            price: Joi.number().required(),
        }),
    }),
};

export { PRODUCT_SCHEMAS };
