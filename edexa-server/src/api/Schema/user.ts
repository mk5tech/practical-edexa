import { celebrate, Joi } from 'celebrate';

const USER_SCHEMAS = {
    REGISTER_USER: celebrate({
        body: Joi.object({
            name:Joi.string(),
            email: Joi.string().email().required(),
            password:Joi.string()
            .min(6)
            .max(10)
            .required(),
        }),
    }),
};

export { USER_SCHEMAS };
