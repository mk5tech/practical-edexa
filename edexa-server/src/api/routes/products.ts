import { Router, Request, Response } from 'express';
import Logger from '../../loaders/logger';
import { isAuth } from '../middleware/isAuth';
import { PRODUCT_SCHEMAS } from '../Schema/product';
import { IProducts } from '../../interfaces/IProducts';

const route = Router();

export default (app: Router) => {
	app.use('/product', route);

    route.post('/',isAuth,addProduct);
    route.get('/my',isAuth,myProductListing);
    route.get('/:id',isAuth, getProductById);
    route.put('/:id',isAuth, editProduct);
    route.delete('/:id',isAuth, deleteProduct);
};

//add product of user
async function addProduct(req: Request, res: Response) {
	const data = req.body;
	IProducts.addProductofUsers(data)
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}


async function editProduct(req: Request, res: Response) {
    const data = req.body;
    const product_id = req.params.id;
	IProducts.editProductofUsers(data,product_id)
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}

async function deleteProduct(req: Request, res: Response) {
    const product_id = req.params.id;
	IProducts.deleteProductById(product_id)
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}

async function myProductListing(req: Request, res: Response) {
	IProducts.myProductListing()
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}

async function getProductById(req: Request, res: Response) {
	const product_id = req.params.id;
	IProducts.getProductById(product_id)
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}
