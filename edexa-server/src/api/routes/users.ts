import { Router, Request, Response } from 'express';

import { IUSers } from '../../interfaces/IUser';
import Logger from '../../loaders/logger';
import { isAuth } from '../middleware/isAuth';
import { USER_SCHEMAS } from '../Schema/user';

const route = Router();

export default (app: Router) => {
	app.use('/user', route);

	route.post('/signup',USER_SCHEMAS.REGISTER_USER, signupUser);
	route.post('/signin',USER_SCHEMAS.REGISTER_USER, signinUser);
	route.get('/info',isAuth, userInfo);
};

//Signup user 
async function signupUser(req: Request, res: Response) {
	const data = req.body;
	IUSers.signupUser(data)
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}

//Signin user 
async function signinUser(req: Request, res: Response) {
	const data = req.body;
	IUSers.signInUser(data)
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}


async function userInfo(req: Request, res: Response) {
	IUSers.userInfo()
		.then(response => {
			res.status(response.status).json(response);
		})
		.catch(e => {
			Logger.error(e);
			res.status(500).json({ status: 500, message: 'Something went wrong' });
		});
}