/* eslint-disable no-console */
import axios, { AxiosRequestConfig, AxiosInstance } from "axios";
import { history } from "./history";
import {API_URL} from './config/const';
axios.interceptors.request.use(async (config) => {
  const token =  localStorage.getItem("authtoken");
  config.baseURL = API_URL;
  config.headers = {
    common: {
      "Access-Control-Allow-Origin": API_URL,
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
    },
  };
  config.headers.authorization = token ? `${token}` : "";
  return config;
});

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.message === "Network Error") {
      error.response = {
        status: 500,
        data: {
          message: "Network Error | Network Unavailable",
        },
      };
    }
    if (error.response.status === 401) {
      //temporory code
      localStorage.removeItem("authtoken");

        history.push("/signin");

    }
    if (error.response.status === 500) {
      if (!error.response.data || !error.response.data.message) {
        error.response = {
          status: 500,
          data: {
            message: "Something went wrong",
          },
        };
      }
    }
    return Promise.reject(error);
  }
);

export default axios;
