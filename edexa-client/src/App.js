import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import '../src/Http'
import Login from "./components/login";
import SignUp from "./components/signup";
import DashBoard from "./components/dashboard";
import AddProduct from "./components/addproduct";


function App() {
  let local_aut_key = null;
  local_aut_key = localStorage.getItem('authtoken');
  return (<Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              {local_aut_key && local_aut_key != undefined ?
                <li className="nav-item">
                  <Link className="nav-link" to={"/dashboard"}>Products</Link>
                </li> : null}
              {!local_aut_key && local_aut_key == undefined ?
              <>
                <li className="nav-item">
                  <Link className="nav-link" to={"/sign-in"}>Login</Link>
                </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
                  </li>
                  </>
                : null}

            </ul>
          </div>
        </div>
      </nav>

      <div className="auth-wrapper">
        <Switch>
          <Route exact path='/' component={Login} />
          <Route exact path="/sign-in" component={Login} />
          <Route exact path="/dashboard" component={DashBoard} />
          <Route exact path="/addproduct/:id" render={(props) => <AddProduct {...props} />} />
          <Route exact path="/addproduct" component={AddProduct} />
          <Route exact path="/sign-up" component={SignUp} />
        </Switch>
      </div>
    </div></Router>
  );
}

export default App;
