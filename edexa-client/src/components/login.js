import React, { Component } from "react";
import axios from 'axios';
import {API_URL} from '../config/const';

export default class SignUp extends Component {
    state = {
        email:'',
        password:'',
        errormsg:null
      }

      componentDidMount(){
        let local_aut_key = null;
        local_aut_key = localStorage.getItem('authtoken');
        if(local_aut_key){
            window.location.href = "/dashboard";  
        }
      }

      handleLogin = event => {
        event.preventDefault();
    
        const data = {
          email:this.state.email,
          password:this.state.password
        };
    
        axios.post(`${API_URL}/user/signin`, data)
          .then(res => {
              console.log(res)
              let data = res.data;
              let token = data.token;
            localStorage.setItem("authtoken",token);
            window.location.href = "/dashboard";
          }).catch((err) => {
          })
      }
    render() {
        return (
            <div className='container'>
                <div className="co-md-6">
            <form onSubmit={this.handleLogin}>
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" placeholder="email"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                     />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password"
                     value={this.state.password}
                     onChange={e => this.setState({ password: e.target.value })}
                    />
                </div>

        <p className="primary">{this.state.errormsg?this.state.errormsg:null}</p>

                <button type="submit" className="btn btn-primary btn-block">Sign In</button>
            </form>
            </div>
            </div>
        );
    }
}