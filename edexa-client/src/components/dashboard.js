import React, { Component } from "react";
import axios from 'axios';
import {API_URL} from '../config/const';

export default class DashBoard extends Component {
    state = {
        productList: [],
        errormsg: null
    }
    componentDidMount() {
        this.setState({ errormsg: null })
        this.getProductList();
    }
    getProductList = event => {
        axios.get(`${API_URL}/product/my`)
            .then(res => {
                let data = res.data.data;
                this.setState({
                    productList: data
                })
                console.log(res)
            }).catch((err) => {
                this.setState({ errormsg: err.response.data.message, productList: [] })
            })
    }
    addProduct() {
        window.location.href = "/addproduct";
    }
    editProduct(id) {
        window.location.href = `/addproduct/${id}`;
    }
    deleteProduct(id) {
        axios.delete(`${API_URL}/product/${id}`)
            .then(res => {
                let data = this.state.productList;
                data = data.filter(d => d._id != id.toString())
                this.setState({
                    productList: data
                })
            }).catch((err) => {
                alert(err.response.data.message)
            })
    }

    render() {
        return (
            <>
                <div className="container">
                    <div className='col-6 p-2' style={{ backgroundColor: "white" }}>
                        <button variant="primary" onClick={this.addProduct}>Add Product</button>
                        {this.state.productList && this.state.productList.length > 0 ?
                            (<table>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.productList.map((product, i) => (
                                        <tr>
                                            <td>{i}</td>
                                            <td>{product.name}</td>
                                            <td>{product.price}</td>
                                            <td>
                                                <button className="ml-2" variant="primary" onClick={() => { this.editProduct(product._id) }}>Edit</button>
                                                <button className="ml-2" variant="primary" onClick={() => { this.deleteProduct(product._id) }}>Delete</button>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>)

                            : <p className="primary">Product Not Found.</p>}
                    </div>

                </div>
            </>
        );
    }
}