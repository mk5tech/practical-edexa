import React, { Component } from "react";
import axios from 'axios';
import {API_URL} from '../config/const';

export default class AddProduct extends Component {
    state = {
        name: '',
        price: '',
        edit:false,
        productid:null,
        errormsg: null
    }

    componentDidMount(){
        let product_id = this.props.match.params.id;
        if(product_id){
            this.getProduct(product_id);
        }
    }

    getProduct = (product_id)=>{
        axios.get(`${API_URL}/product/${product_id}`)
            .then(res => {
                let data = res.data.data;
                this.setState({edit:true,name:data.name,price:data.price,productid:data._id});
            }).catch((err) => {
                this.setState({errormsg:err.response.data.message})
            })
    }

    addProduct = event => {
        event.preventDefault();

        const data = {
            name: this.state.name,
            price: this.state.price
        };

        axios.post(`${API_URL}/product`, data)
            .then(res => {
                window.location.href = "/dashboard";
            }).catch((err) => {
                this.setState({errormsg:err.response.data.message})
            })
    }

    editProduct = event => {
        event.preventDefault();

        const data = {
            name: this.state.name,
            price: this.state.price
        };

        axios.put(`${API_URL}/product/${this.state.productid}`, data)
            .then(res => {
                window.location.href = "/dashboard";
            }).catch((err) => {
                this.setState({errormsg:err.response.data.message})
            })
    }
    render() {
        let edit = this.state.edit;
        return (
            <div className='container'>
                <div className="co-md-6">
                    <form onSubmit={edit?this.editProduct:this.addProduct}>
                        <h3>Add Product</h3>

                        <div className="form-group">
                            <label>Name</label>
                            <input type="text" className="form-control" placeholder="name"
                                value={this.state.name}
                                onChange={e => this.setState({ name: e.target.value })}
                            />
                        </div>

                        <div className="form-group">
                            <label>Price</label>
                            <input type="text" className="form-control" placeholder="Enter Price"
                                value={this.state.price}
                                onChange={e => this.setState({ price: e.target.value })}
                            />
                        </div>

                        <p className="primary">{this.state.errormsg ? this.state.errormsg : null}</p>

                        <button type="submit" className="btn btn-primary btn-block">{edit?'Edit':'Add'}  Product</button>
                    </form>
                </div>
            </div>
        );
    }
}