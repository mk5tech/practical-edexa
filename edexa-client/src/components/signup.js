import React, { Component } from "react";
import axios from 'axios';
import {API_URL} from '../config/const';

export default class SignUp extends Component {
    state = {
        name:'',
        email:'',
        password:'',
        errormsg:null
      }

      componentDidMount(){
        let local_aut_key = null;
        local_aut_key = localStorage.getItem('authtoken');
        if(local_aut_key){
            window.location.href = "/dashboard";  
        }
      }

      handleRegister = event => {
        event.preventDefault();
    
        const data = {
          name: this.state.name,
          email:this.state.email,
          password:this.state.password
        };
    
        axios.post(`${API_URL}/user/signup`, data)
          .then(res => {
            window.location.href = "/sign-in";
          }).catch((err) => {
            this.setState({
                errormsg:err.response.data.message
            })
          })
      }
    render() {
        return (
            <div className='container'>
                <div className="co-md-6">
            <form onSubmit={this.handleRegister}>
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label>name</label>
                    <input type="text" className="form-control" placeholder="First name" 
                    value={this.state.name}
                    onChange={e => this.setState({ name: e.target.value })}
                    />
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" placeholder="email"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                     />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password"
                     value={this.state.password}
                     onChange={e => this.setState({ password: e.target.value })}
                    />
                </div>

        <p className="primary">{this.state.errormsg?this.state.errormsg:null}</p>

                <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
            </form>
            </div>
            </div>
        );
    }
}